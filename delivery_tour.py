# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.model import (
    DeactivableMixin, ModelView, ModelSQL, fields, sequence_ordered)
from trytond.pyson import Eval

__all__ = ['Tour', 'Station', 'Van', 'StationAddressRelation']


class Station(DeactivableMixin, sequence_ordered(), ModelSQL, ModelView):
    'Station for a delivery tour'
    __name__ = 'delivery_tour.station'

    #stops = fields.One2Many('delivery_tour.station', 'station',
    #                        'Stops')

    party = fields.Many2One('party.address', "Party", required=True)
    shipment_address = fields.Many2One('party.address', 'Shipment Address',
                                       domain=[('party', '=', Eval('party'))],
                                       depends=['party'])
    # TODO: capacity
    published_address = fields.Char(
        "Published Address Text",
        help="Address published on the public website")
    driver_info = fields.Char("Driver Info",
                              help=("Information for the driver "
                                    "about this station, "
                                    "printed on the tour sheet"))
    comment = fields.Char("Comment",
                          help="Internal comment for this station")
    sequence = fields.Integer('Sequence')
    tour = fields.Many2One('delivery_tour', 'Tour',
                           ondelete='RESTRICT')  # can't delete while in tour
    # TODO: contact = party? contact method?


    # @fields.depends('party')
    # def on_change_party(self):
    #     if self.party:
    #         self.shipment_address = self.party.address_get(type='delivery')
    #     else:
    #         self.shipment_address = None


class StationAddressRelation(ModelSQL):
    'Station - Address relation'
    __name__ = 'delivery_tour.station-party.address'

    station = fields.Many2One('delivery_tour.station', 'Station', required=True,
        ondelete='CASCADE')
    address = fields.Many2One('party.address', 'Address', required=True,
        ondelete='RESTRICT')


## class TourStop(ModelSQL, ModelView):
##     'Station stop on a tour'
##     __name__ = 'delivery_tour-delivery_tour.station'

##     station = fields.Many2One('delivery_tour.station', 'Station',
##                               ondelete='RESTRICT', required=True)
##     tour = fields.Many2One('delivery_tour', 'Tour',
##                            ondelete='CASCADE', required=True)

##     driver_info = fields.Char("Driver Info",
##                               help=("Information for the driver "
##                                     "about this tour-stop, "
##                                     "printed on the tour sheet"))
##     comment = fields.Char("Comment",
##                           help="Internal comment for this tour stop")
##     sequence = fields.Integer('Sequence')


class Van(DeactivableMixin, ModelSQL, ModelView):
    'Van used for delivery'
    __name__ = 'delivery_tour.van'

    tours = fields.One2Many('delivery_tour', 'van', 'Tour',
                            required=False)

    name = fields.Char("Name", required=True)
    # capacity
    # max_payload_weight


class Tour(DeactivableMixin, ModelSQL, ModelView):
    'Delivery Tour'
    __name__ = 'delivery_tour'

    stops = fields.One2Many('delivery_tour.station', 'tour', 'Stops')
    van = fields.Many2One('delivery_tour.van', 'Van')

    name = fields.Char("Name", required=True)
    description = fields.Char("Description", help="Brief description")
    weekday = fields.Many2One('ir.calendar.day', "Day of Week")
    # Tourenpunkte mit Riehenfolge
